<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class Post extends Model
{
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'title' ,'body' ,'image' ,'user_id'
    ];

    protected $appends = ['image_url'];

    /**
    * Upload destination for posts
    */
    public $upload_distination = '/uploads/images/posts/';

    /**
    * Accessors & Mutators
    */
    public function setImageAttribute($value)
    {
        if (!$value instanceof UploadedFile) {
            $this->attributes['image'] = $value; // for testing purposes when using faker
            return;
        }
        $image_name = str_random(60);
        $image_name = $image_name.'.'.$value->getClientOriginalExtension();
        $value->move(public_path($this->upload_distination),$image_name);
        $this->attributes['image'] = $image_name;
    }

    public function getImageUrlAttribute()
    {
        if ($this->image) {
            //check if it's a link or just an encoded name
            return strpos($this->image ,'http') !== FALSE ? $this->image : asset($this->upload_distination.$this->image);
        }
        return '';
    }

    /**
    * Relations
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
