<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('web.newsfeed');
    }

    public function posts(Request $request)
    {
        $posts = Post::orderBy('id' ,'DESC')->paginate(2);
        return response()->json($posts);
    }
}
