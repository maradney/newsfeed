<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\AdminCreateRequest;
use App\Http\Requests\Dashboard\AdminUpdateRequest;
use App\User;

class AdminController extends Controller
{
    protected $base_view_path = 'dashboard.admins.';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $index = request()->get('page', 1);
        $data['counter_offset'] = ($index * 20) - 20;
        $data['resources'] = User::orderBy('id', 'DESC')->paginate(20);
        $data['total_resources_count'] = User::count();
        return view($this->base_view_path . 'index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->base_view_path . 'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminCreateRequest $request)
    {
        $data = $request->all();

        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);

        alert()->success('Admin created successfully.', 'Success');
        return redirect()->route('dashboard.admins.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $admin)
    {
        $data['resource'] = $admin;
        return view($this->base_view_path . 'show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $admin)
    {
        $data['resource'] = $admin;
        return view($this->base_view_path . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminUpdateRequest $request, User $admin)
    {
        $data = $request->all();

        if ($request->has('password') && !$data['password']) {
            unset($data['password']);
        }elseif ($request->has('password')) {
            $data['password'] = bcrypt($data['password']);
        }
        $admin->update($data);

        alert()->success('Admin updated successfully.', 'Success');
        return redirect()->route('dashboard.admins.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $admin)
    {
        $admin->delete();
        return response()->json([
            'statusCode' => 200,
            'message' => 'Admin deleted successfully.',
        ],200);
        // alert()->success('Admin deleted successfully.', 'Success');
        // return redirect()->route('dashboard.admins.index');
    }
}
