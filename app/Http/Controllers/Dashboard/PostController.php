<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\PostCreateRequest;
use App\Http\Requests\Dashboard\PostUpdateRequest;
use App\Events\PostCreateEvent;
use App\Post;

class PostController extends Controller
{
    protected $base_view_path = 'dashboard.posts.';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $index = request()->get('page', 1);
        $data['counter_offset'] = ($index * 20) - 20;
        $data['resources'] = Post::orderBy('id', 'DESC')->paginate(20);
        $data['total_resources_count'] = Post::count();
        return view($this->base_view_path . 'index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->base_view_path . 'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostCreateRequest $request)
    {
        $data = $request->all();

        $post = Post::create($data);
        event(new PostCreateEvent($post));

        alert()->success('Post created successfully.', 'Success');
        return redirect()->route('dashboard.posts.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $data['resource'] = $post;
        return view($this->base_view_path . 'show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $data['resource'] = $post;
        return view($this->base_view_path . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostUpdateRequest $request, Post $post)
    {
        $data = $request->all();

        if ($request->has('image')) {
            $file_path = public_path($post->upload_distination.$post->image);
            if ($post->image && file_exists($file_path)) {
                unlink($file_path);
            }
        }
        $post->update($data);

        alert()->success('Post updated successfully.', 'Success');
        return redirect()->route('dashboard.posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $file_path = public_path($post->upload_distination.$post->image);
        if ($post->image && file_exists($file_path)) {
            unlink($file_path);
        }
        $post->delete();
        return response()->json([
            'statusCode' => 200,
            'message' => 'Post deleted successfully.',
        ],200);
    }
}
