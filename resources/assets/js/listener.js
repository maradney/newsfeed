import Echo from "laravel-echo"

/**
 * Load pusher library
 */
window.Pusher = require('pusher-js');

/**
 * Intialize laravel echo
 */
window.Echo = new Echo({
    broadcaster: 'pusher',
    key: 'c7cbbaff05ddb18f34c2',
    cluster: 'eu',
    encrypted: true
});
/**
 * Test channel
 */
window.Echo.channel('posts-channel')
.listen("PostCreateEvent", (e) => {
    console.log(e);
});

/**
 * Load vue library
 */
window.Vue = require('vue');
/**
 * Load axios library
 */
 window.axios = require('axios');
 window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Create shared vue instance
 */
window.Event = new Vue();


Vue.component('posts', require('./components/posts.vue'));

/**
 * Intialize vue app
 */
const app = new Vue({
    el: '#app',
    data() {
        return {
            loading_screen : false,
        }
    },
    created() {
        var vm = this;
        Event.$on('show-loading-screen' , function() {
            vm.loading_screen = true;
        });
        Event.$on('hide-loading-screen' , function() {
            vm.loading_screen = false;
        });
    }
});
