<header id="sidebar">
    <div class="l-sidebar" id="side">
        <div class="logo ">
            <div class="logo__txt ">
                <a href="#">
                    <img src="{{ asset('panel-assets/images/icons/01_icon.png') }}" class="img-responsive hamburger-toggle js-hamburger" />
                </a>
            </div>
        </div>
        <div class="l-sidebar__content">
            <nav class="c-menu js-menu">
                <ul class="u-list">
                    <?php $route = request()->route(); ?>
                    <li class="c-menu__item {{ strpos($route->getName() ,'dashboard.index') !== FALSE ? 'is-active' : '' }}" data-toggle="tooltip" title="Dashboard">
                        <a href="{{ route('dashboard.index') }}">
                            <div class="c-menu__item__inner">
                                <i class="fa fa-dashboard"></i>
                                <div class="c-menu-item__title">
                                    <span>
                                        Dashboard
                                    </span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="c-menu__item {{ strpos($route->getName() ,'dashboard.posts') !== FALSE ? 'is-active' : '' }}" data-toggle="tooltip" title="Posts">
                        <a href="{{ route('dashboard.posts.index') }}">
                            <div class="c-menu__item__inner">
                                <i class="fa fa-comment"></i>
                                <div class="c-menu-item__title">
                                    <span>
                                        Posts
                                    </span>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>
