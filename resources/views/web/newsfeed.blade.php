<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Newsfeed posts</title>
    <!-- Bootstrap core CSS -->
    {{ Html::style('web-assets/bootstrap/css/bootstrap.css') }}
    <!-- Custom styles for this template -->
    {{ Html::style('web-assets/navbar.css') }}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
    .spinner-container {
        position: fixed;
        background-color: #0009;
        width: 100%;
        height: 110%;
        z-index: 10000;
    }

    @keyframes spinner {
        to {transform: rotate(360deg);}
    }
    .spinner {
        content: '';
        box-sizing: border-box;
        position: relative;
        top: 37%;
        left: 46%;
        height: 150px;
        width: 150px;
        margin-top: -15px;
        margin-left: -15px;
        border-radius: 50%;
        border: 1px solid #ccc;
        border-top-color: #07d;
        animation: spinner .6s linear infinite;
    }
    </style>
</head>
<body>
    <div id="app">
        <div class="spinner-container" v-show="loading_screen" style="display:none;"><div class="spinner"></div></div>
        <div class="container">
            <!-- Static navbar -->
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="">{{ config('app.name') }}</a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a href="#">Home</a>
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
                <!--/.container-fluid -->
            </nav>
            <!-- Main component for a primary marketing message or call to action -->
            
            <posts url="posts">

            </posts>
        </div>
    </div>

    <!-- /container -->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    {{ Html::script('web-assets/assets/js/jquery.min.js') }}
    {{ Html::script('web-assets/bootstrap/js/bootstrap.min.js') }}
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    {{ Html::script('assets/js/ie10-viewport-bug-workaround.js') }}

    {{ Html::script('js/listener.js') }}
</body>
</html>
