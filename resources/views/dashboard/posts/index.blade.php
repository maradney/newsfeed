@extends('layouts.dashboard.app')

@section('stylesheets')
<style>
body {
    background-color:#edeff9;
}
</style>
@stop

@section('section-title')
<div class="row">
    <div class="col-md-4 col-xs-12">
        <h3 class="section-title contacts-section-title">
            Posts
        </h3>
    </div>
    <div class="col-md-8 col-md-offset-4 text-right col-xs-11">
        <!-- <a href="{ route('dashboard.posts.download' ,'csv') }}"class="btn btn-blue margin-left-10">
            To excel
        </a> -->
        <!-- <a href="{ route('dashboard.posts.download' ,'pdf') }}"class="btn btn-blue margin-left-10">
            To PDF
        </a> -->
        <a href="{{ route('dashboard.posts.create') }}"class="btn btn-blue margin-left-10">
            <span>+ </span>Create post
        </a>
    </div>
</div>
@stop

@section('content')
<div class="row">
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-5 margin-bottom10 margin-top20">
                <div class="total-customer-col pad5 pad-bottom5 col-md-12">
                    <div class="col-md-9 customer-stat-col-pad">
                        <h5 class="customer-stat-text pad5">Posts count</h5>
                    </div>
                    <div class="col-md-3 text-center">
                        <h5 class="customer-stat-num pad5">
                            {{ $total_resources_count }}
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row margin-top15">
    <div class="col-md-12">
        <div class="row margin-bottom10">
            {{ $resources->links() }}
        </div>
        <div class="row margin-bottom10 contacts-list-view-card pad15">
            <table class="table table-borderless table-responsive" style="margin-bottom:0;">
                <tbody>
                    <tr>
                        <th class="text-center" style="width:5%;">#</th>
                        <th style="width:20%;">
                            Image
                        </th>
                        <th style="width:20%;">
                            Title
                        </th>
                        <th style="width:50%;">
                            Body
                        </th>
                        <th></th>
                    </tr>
                    @foreach($resources as $resource)
                    <tr class="">
                        <td class="text-center">
                            <h3 class="contact-list-view-column-categ margin-top10 contact-details-view" style="font-weight: 400;">
                                {{ $counter_offset + $loop->iteration }}
                            </h3>
                        </td>
                        <td>
                            <img src="{{ $resource->image_url }}" alt="Not found" class="thumbnail" style="width:100%;">
                        </td>
                        <td>
                            <h3 class="contact-list-view-column-categ margin-top10 contact-details-view" style="font-weight: 400;">
                                <a href="{{ route('dashboard.posts.show', $resource->id) }}">
                                    {{ $resource->title }}
                                </a>
                            </h3>
                        </td>
                        <td>
                            <h3 class="margin-top10 contact-details-view" style="font-weight: 400;">
                                {{ $resource->body }}
                            </h3>
                        </td>
                        <td>
                            <div class="no-shadow btn-group pull-right" style="margin:0;padding:0;">
                                <button type="button" class="btn btn-sm edit-btn text-center margin-left-10 dropdown-toggle contact-edit-dots-shdw pad0" data-toggle="dropdown">
                                    <i class="fa fa-ellipsis-h fa-lg edit-btn-contact-ico-color"></i>
                                </button>
                                <ul class="dropdown-menu contact-dropdown pull-right">
                                    <li>
                                        <a href="{{ route('dashboard.posts.edit', $resource->id) }}">
                                            Edit
                                        </a>
                                    </li>
                                    <li>
                                        {{ Form::open(['route' => ['dashboard.posts.destroy' ,$resource->id] ,'method' => 'DELETE' ,'class' => 'delete-form']) }}
                                        <button type="submit">
                                            Delete
                                        </button>
                                        {{ Form::close() }}
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
@section('scripts')
<script>
$(document).ready(function(){
    $('.delete-form').submit(function(e){
        e.preventDefault();
        var form = $(this);
        var url = $(this).attr('action');
        $.ajax({
            url: url,
            method: 'POST',
            data: form.serialize(),
            dataType: 'json',
            success: function(response){
                form.parents('tr').remove();
                swal({
                    title: "Success",
                    text: response.message,
                    type: "success"
                });
            }
        });
    });
});
</script>
@stop
