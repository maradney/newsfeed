
@extends('layouts.dashboard.app')

@section('stylesheets')
<style>
body {
    background-color:#edeff9;
}
</style>
@stop

@section('section-title')
<div class="row">
    <div class="col-md-4 col-xs-12">
        <h3 class="section-title contacts-section-title">
            Post Info.
        </h3>
    </div>
    <div class="col-xs-12 col-md-3">
        <div class="row">
            <div class="col-md-4 sort-col col-xs-4">
            </div>
            <div class="col-md-3 contact-edit-col col-xs-4">
            </div>
        </div>
    </div>
    <div class="col-md-4 col-md-offset-1 text-right col-xs-11">
        <a href="{{ route('dashboard.posts.create') }}"class="btn btn-primary margin-left-10">
            <span>+ </span>Add post
        </a>
        <a href="{{ route('dashboard.posts.edit',$resource->id) }}"class="btn btn-warning margin-left-10">
            Edit post
        </a>
    </div>
</div>
@stop

@section('content')
<div class="row margin-top15">
    <div class="col-md-12">
        <div class="row margin-bottom10 contacts-list-view-card pad15">
            <table class="table table-borderless table-responsive" style="">
                <tbody>
                    <tr>
                        <th class="text-center" style="width:100px;">
                            ID
                        </th>
                        <td>
                            {{ $resource->id }}
                        </td>
                    </tr>
                    <tr>
                        <th class="text-center" style="width:100px;">
                            Title
                        </th>
                        <td>
                            @if($resource->image)
                            <img src="{{ $resource->image_url }}" alt="Upload image" class="thumbnail" style="width:100%;">
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th class="text-center" style="width:100px;">
                            Title
                        </th>
                        <td>
                            {{ $resource->title }}
                        </td>
                    </tr>
                    <tr>
                        <th class="text-center" style="width:100px;">
                            Body
                        </th>
                        <td>
                            {{ $resource->body }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
