@extends('layouts.dashboard.app')

@section('section-title')
<div class="row">
    <div class="col-md-4 col-sm-12">
        <h3 class="section-title">Create new post</h3>
    </div>
</div>
@stop

@section('content')
{{ Form::open(['route' => 'dashboard.posts.store' ,'files' => true]) }}
<div class="row">
    <div class="col-md-12">
        <h3 class="secondry-title">Post Info.</h3>
    </div>
    <div class="col-md-12">
        <div class="form-group margin-bottom20 col-md-6">
            <label class="control-label" for="image" style="width:100%;">
                <span class="text-danger">*</span>
                Image
            </label>
            <div class="col-md-6">
                <label for="image" style="width:100%;">
                    <img src="{{ asset('panel-assets/images/profile-picutre/01_img.png') }}" alt="Upload image" class="thumbnail" style="width:100%;cursor: pointer; cursor: hand;">
                    <input type="file" id='image' name='image' style="display:none;" onchange="preview(this)";>
                </label>
                <p class="text-danger" style="margin-bottom: 0;">{{ $errors->first('image') }}</p>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group margin-bottom20 col-md-12">
            <label class="control-label" for="title">
                <span class="text-danger">*</span>
                Title
            </label>
            {{ Form::text('title',old('title'),['id'=>'title','max' => 255 ,'required'=>'required','class' => 'form-control']) }}
            <p class="text-danger" style="margin-bottom: 0;">{{ $errors->first('title') }}</p>
        </div>
        <div class="form-group margin-bottom20 col-md-12">
            <label class="control-label" for="body">
                <span class="text-danger">*</span>
                Body
            </label>
            {{ Form::textarea('body',old('body'),['id'=>'body','required'=>'required','class' => 'form-control']) }}
            <p class="text-danger" style="margin-bottom: 0;">{{ $errors->first('body') }}</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-1 col-xs-4">
        <button type="submit" class="btn primary-btn">
            Save
        </button>
    </div>
    <div class="col-md-1 col-xs-4">
        <button type="reset" class="btn cancel-btn">
            Reset
        </button>
    </div>
</div>
{{ Form::close() }}
@stop

@section('scripts')
<script type="text/javascript">
function preview(input)
{
    var parent = $(input).parent();
    var preview = parent.find('img');
    var file    = input.files[0];
    var reader  = new FileReader();

    reader.addEventListener("load", function () {
        preview.attr('src',reader.result);
    }, false);

    if (file) {
        reader.readAsDataURL(file);
    }
}
</script>
@stop
