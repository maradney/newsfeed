<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('index');
Route::get('posts', 'HomeController@posts')->name('posts');

$dashboard_options = [
    'prefix' => 'dashboard',
    'as' => 'dashboard.',
    'namespace' => 'Dashboard',
    'middleware' => [
        'auth:web'
    ],
];

Route::group($dashboard_options, function () {
    Route::get('/', 'HomeController@index')->name('index');

    // Route::resource('admins', 'AdminController');
    Route::resource('posts', 'PostController');
});
